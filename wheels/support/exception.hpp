#pragma once

#include <string>

namespace wheels {

std::string CurrentExceptionMessage();

}  // namespace wheels
