#pragma once

#include <wheels/test/reporter.hpp>

namespace wheels::test {

ITestReporterPtr GetConsoleReporter();

}  // namespace wheels::test
