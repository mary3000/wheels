#pragma once

#include <wheels/test/test.hpp>

namespace wheels::test {

void ExecuteTest(ITestPtr test);

}  // namespace wheels::test
