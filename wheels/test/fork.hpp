#pragma once

#include <wheels/test/test.hpp>

namespace wheels::test {

void ExecuteTestWithFork(ITestPtr test);

}  // namespace wheels::test
